let mongoose  = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;


let UserSchema = new Schema({
    // name: {type: String, default: null },
    role: {type: String, default: 'user', enum:['admin', 'user'] },
    email: {type: String, default: null },
    username: {type: String, default: null },
    profileImageURL: {type: String, default: null },
    phone: {type: String, default: null },
    password: {type: String, default: null },
    prizebonds:[
        {
            value:{type: String, default: null },
            number:{type: String, default: null },
            is_winner:{type: Boolean, default: false },
            winning_count:{type: String, default: 0 },
            updated_at: {type: Date, default: new Date()},
            created_at: {type: Date, default: new Date()},
        }
    ],
    active:{type: Boolean, default: true },
    updated_at: {type: Date, default: new Date()},
    created_at: {type: Date, default: new Date()},
});

mongoose.model('User', UserSchema);
let Users=  mongoose.model('User');

Users.findOne({}, (err, result)=>{
    if(err)
        console.log(err);
    if (!result) {
        Users.create({name: "Admin", role:'admin', email: 'admin@admin.com', username:'admin', password:"1fe176f24084d573ade99d9a1098d9679da82eec53a371dcce8ce60f082f218d6bd8e76042a6505cf4330fa8c24e9181ee0a7a09d7cdf0f72de99a6ec791ce7f"}, (err, product, numAffected)=>{
            if(err)
                console.log(err)
        });
    }
}) 