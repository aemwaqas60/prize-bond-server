let mongoose  = require('mongoose');
let  Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;


let SettingSchema = new Schema({

    denominations: {type: Array, default: []},
    
    updated_at: {type: Date, default: new Date()},
    created_at: {type: Date, default: new Date()},
});


mongoose.model('Settings', SettingSchema);
let Settings = mongoose.model('Settings');

Settings.findOne({}, (err, result)=>{
    if(err)
        console.log(err);
    if (!result) {
        Settings.create({}, (err, product, numAffected)=>{
            if(err)
                console.log(err)
        });
    }
}) 
