let mongoose  = require('mongoose');
let  Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;


let DrawsSchema = new Schema({

    name: {type: String, default: null },
    description: {type: String, default: null },
    held_at:{type: String, default: null },
    value:{type: String, default: null },
    draw_number: {type: String, default: null },
    series: {type: String,default: null},
    fileOriginalName: {type: String,default: null},
    extn: {type: String,default: null},
    date:{type: String,default: null},
    is_processed:{type: Boolean, default: false },
    prize:[{
        value:{type: String,default: null},
        number:{type: Array,default: []},
    }],
    is_delete: {type: Boolean, default: false },
    created_at: {type: Date, default:+new Date()}
});

mongoose.model('Draws', DrawsSchema);