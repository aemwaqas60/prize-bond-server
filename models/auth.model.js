let mongoose  = require('mongoose');
let  Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

let AuthSchema = new Schema({

    _user: { type: ObjectId, ref: 'User' },
    token: {type: String, default: null },
    is_delete: {type: Boolean, default: false },
    player_id:{type: String, default: null },
    updated_at: {type: Date, default:+new Date()},
    created_at: {type: Date, default:+new Date()}
});

mongoose.model('Auth', AuthSchema);