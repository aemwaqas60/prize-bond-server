const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');


const dbURI = process.env.dbURI || 'mongodb://127.0.0.1:27017/savings';

mongoose.connect(dbURI);   

// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('connected', function () {  
  console.log('Mongoose default connection open to ' + dbURI);
}); 

// If the connection throws an error
mongoose.connection.on('error',function (err) {  
  console.log('Mongoose default connection error: ' + err);
  //mongoose.disconnect();
}); 

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {  
  console.log('Mongoose default connection disconnected'); 
  //mongoose.connect(dbURI, {server:{auto_reconnect:true}});    // db will auto connect
  
});

// If the Node process ends, close the Mongoose connection 
process.on('SIGINT', function() {  
  mongoose.connection.close(function () { 
    console.log('Mongoose default connection disconnected through app termination'); 
    process.exit(0); 
  }); 
}); 