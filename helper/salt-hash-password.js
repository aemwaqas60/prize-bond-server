'use strict';
var crypto = require('crypto');


class Salthashpassword{

    constructor(salt){
        this.salt = salt
    }
    genRandomString(length){
        return crypto.randomBytes(Math.ceil(length/2))
                .toString('hex') /** convert to hexadecimal format */
                .slice(0,length);   /** return required number of characters */
    };
    sha512(password){
        var hash = crypto.createHmac('sha512', this.salt); /** Hashing algorithm sha512 */
        hash.update(password);
        var value = hash.digest('hex');
        return {
            salt:this.salt,
            passwordHash:value
        };
    };
    saltHashPassword(userpassword, cb){
        var passwordData = this.sha512(userpassword);
        //console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=')
        //console.log('UserPassword = '+userpassword);
        //console.log('Passwordhash = '+passwordData.passwordHash);
        //console.log('nSalt = '+passwordData.salt);
        return cb(null, passwordData.passwordHash);
    };

}
module.exports = Salthashpassword;





/**
 * generates random string of characters i.e salt
 * @function
 * @param {number} length - Length of the random string.
 */
var genRandomString = function(length){
    return crypto.randomBytes(Math.ceil(length/2))
            .toString('hex') /** convert to hexadecimal format */
            .slice(0,length);   /** return required number of characters */
};

/**
 * hash password with sha512.
 * @function
 * @param {string} password - List of required fields.
 * @param {string} salt - Data to be validated.
 */
var sha512 = function(password, salt){
    var hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hash.update(password);
    var value = hash.digest('hex');
    return {
        salt:salt,
        passwordHash:value
    };
};
/*
module.exports = {
    saltHashPassword:(userpassword, cb)=> {
        //var salt = genRandomString(16);
        let salt = process.env.nsalt;
        var passwordData = sha512(userpassword, salt);
        //console.log('UserPassword = '+userpassword);
        //console.log('Passwordhash = '+passwordData.passwordHash);
        //console.log('nSalt = '+passwordData.salt);
        return cb(null, passwordData.passwordHash);
    }
}*/