'use strict';

let _self;
class Authentication {

    constructor(jwt, jwtSecret, _) {
        _self = this;
        this.jwtSecret = jwtSecret;
        this.jwt = jwt;
        this._ = _;

    }
    isAuthenticated(req, res, next) {
        console.log('-------==== ADMIN AUTHENTICATION====----=-=-=-=-=-=-=-=')
        if (_self._.isEmpty(req.signedCookies)) {
            let err = new Error('you are not authorized for this action.');
            err.status = 401;
            return next(err);
        }

        let token = req.signedCookies._connecti;
        _self.jwt.verify(token, _self.jwtSecret, function (err, decoded) {
            if (err) {
                let err = new Error('you are not authorized for this action.');
                err.status = 401;
                return next(err);
            }
            //return res.status(401).clearCookie('_connecti', { path: '/' }).json({status:401, status_message:"you are authorized for this action."})
            req.user = decoded.data;
            next();
        });
    }

}
module.exports = Authentication;