'use strict';

require('../config/mongo');
require('../models/user.model');
require('../models/draws.model');
require('../models/settings.model');
require('../models/auth.model');
const path = require('path');
 const   main_folder_path = path.join(__dirname, '../files/images/');   
const file_handler = require('../helper/file_handler');

const _ = require('underscore');
const Q = require('q');

let _self;
class apis{

    constructor(mongoose, ObjSalthashpassword, jwt, jwtSecret){
        _self = this;
        this.Users = mongoose.model('User');
		this.Draws = mongoose.model('Draws');
		this.Settings = mongoose.model('Settings');
        this.Auth = mongoose.model('Auth');
        this.ObjSalthashpassword = ObjSalthashpassword;
        this.jwtSecret = jwtSecret;
        this.jwt = jwt;
    }

    _isAuthenticate(req, res, next){
        let token = req.get('token');
        console.log(token)
        _self.Auth.find({token}, '-token').populate('_user', '-password').lean().exec((err, doc)=>{
            if(_.isEmpty(doc) === true)
                return res.json({status:401, status_message: 'user unauthorized'});
                // return res.json({status:401, status_message: 'user unauthorized', data:[]});
        
            res.locals.user = doc[0]._user;
            next();
        });
    }

    signup(req, res, next){
       
       
        console.log("inside signup");
       // console.log(req.body);

        let {name='', username='', email='', phone='', password=''} = req.body;

        console.log({name, username, email, phone, password});
        if(name === '' || username === '' || email === '' || phone ==='' || password ==='')
            res.json({status:422, status_message:'missing_field'});
            //res.json({status:422, status_message:'missing_field', data:[]});

        _self.Users.find({phone}).lean().exec((err, doc)=>{
            console.log(doc)
            if(_.isEmpty(doc) === false)
                return res.json({status:400, status_message:'user_already_exist'});
                // return res.json({status:400, status_message:'user_already_exist', data:[]});
            
            let user = {    
                name,
                email,
                username,
                phone,
                role:'user'
            }
            
            _self.ObjSalthashpassword.saltHashPassword(password, (err, result)=>{
                if(err)
                    return res.json({status:500, status_message:'something went wrong | internal server error'});
                    // return res.json({status:500, status_message:'something went wrong | internal server error', data:[]});

                user.password = result;
                let objuser = new _self.Users(user);
                objuser.save((err, doc, numAffected)=>{
                    if(err)
                    {
                        console.log("inside signup");
                        return res.json({err, msg:"error in query"});
                    }
                        
                    let leanObj = doc.toObject();
                    delete leanObj.password;
                    require('crypto').randomBytes(50, function(err, buffer) {
                         let token = buffer.toString('hex');
                         leanObj.token = token;
                         let auth = {
                             _user : leanObj._id,
                             token,
                         }
                         let authobj = new _self.Auth(auth);
                         authobj.save((err, _doc)=>{
                            if(err)
                                return res.json({err, msg:"error in query"});

                            res.json({status:200, status_message:'ok', data:leanObj});
                        }); 
                    });
                        
                    //return res.json({status:200, status_message:'ok', data:leanObj});

                });
            })
        })

    }

    signin(req, res, next){
        let {email='', password='', phone = ''} = req.body;
        console.log({email, password, phone});
        if(password ==='' || phone ===``)
            res.json({status:422, status_message:'missing_field'});
            // res.json({status:422, status_message:'missing_field', data:[]});

        _self.ObjSalthashpassword.saltHashPassword(password, (err, result)=>{
                if(err)
                    return res.json({status:500, status_message:'something went wrong | internal server error'});
                    // return res.json({status:500, status_message:'something went wrong | internal server error', data:[]});

                let password = result;
                _self.Users.find({phone, password}).lean().exec((err, doc)=>{

                    if(_.isEmpty(doc) === true)
                        return res.json({status:400, status_message:'user phone or password incorrect | please try again'});
                        // return res.json({status:400, status_message:'user phone or password incorrect | please try again', data:[]});
                    doc = doc[0];
                    delete doc.password;
                    require('crypto').randomBytes(50, function(err, buffer) {
                         let token = buffer.toString('hex');
                         doc.token = token;
                         let auth = {
                             _user : doc._id,
                             token,
                         }
                         let authobj = new _self.Auth(auth);
                         authobj.save((err, _doc)=>{
                            if(err)
                                return res.json({err, msg:"error in query"});

                            res.json({status:200, status_message:'ok', data:doc});
                        }); 
                    });
                });
        });
    }

    getuserprofile(req, res, next){
        let user = res.locals.user;

        _self.Users.find(
            {_id:user._id}
        )
        .lean()
        .exec((err, doc)=>{
             if(err)
                return res.json({status:500, status_message:'something went wrong | internal server error'});
                // return res.json({status:500, status_message:'something went wrong | internal server error', data:[]});

            delete doc[0].password;
            return res.json({status:200, status_message: 'success', data:doc});
        })
    }

    _edituserprofile(req, res, next){
        let user = res.locals.user;
        let {name='', username='', phone='', password=``} = req.body;

        let obj = {};

        if(name !== ``)
            obj.name = name;
        if(username !== ``)
            obj.username = username;
        if(phone !== ``)
            obj.phone;
        if(password !== ``){
            _self.ObjSalthashpassword.saltHashPassword(password, (err, result)=>{
                if(err)
                    return res.json({status:500, status_message:'something went wrong | internal server error'});
                    // return res.json({status:500, status_message:'something went wrong | internal server error', data:[]});
                
                obj.password = result;
            });
        }
        



        _self.Users.findOneAndUpdate(
            {_id:user._id},
            obj,
            {safe: true, upsert: false, new : true}
        )
        .lean()
        .exec((err, doc)=>{
            if(err)
                return res.json({status:500, status_message:'something went wrong | internal server error'});
                // return res.json({status:500, status_message:'something went wrong | internal server error', data:[]});

            delete doc.password;
            return res.json({status:200, status_message: 'success', data:doc});
        })


    }

    edituserprofile(req, res, next){
        let user = res.locals.user;
        console.log(req.tempfilename)

        if(req.tempfilename){
            file_handler.file_rename(req.tempfilename, main_folder_path+user._id+req.tempextn, (err, result)=>{

                if(err)
                    return res.json({status:500, status_message:'something went wrong | internal server error | image rename error'+err});
                    // return res.json({status:500, status_message:'something went wrong | internal server error | image rename error'+err, data:[]});
                
                _self.edituserprofile_general(req, res, next, user, main_folder_path+user._id+req.tempextn);

            });
        }
        else{
            _self.edituserprofile_general(req, res, next, user);
        }

    }

    edituserprofile_general(req, res, next, user, image=``){
        
        console.log('there')

            let {name='', username='', phone='', password=``} = req.body;

            let obj = {};
            if(name !== ``)
                obj.name = name;
            if(username !== ``)
                obj.username = username;
            if(phone !== ``)
                obj.phone = phone;
            if(image !== ``)
                obj.profileImageURL = image;
            if(password !== ``){
                _self.ObjSalthashpassword.saltHashPassword(password, (err, result)=>{
                    if(err)
                        return res.json({status:500, status_message:'something went wrong | internal server error'});
                        // return res.json({status:500, status_message:'something went wrong | internal server error', data:[]});
                    
                    obj.password = result;
                });
            } 

            console.log(obj) 

            _self.Users.findOneAndUpdate(
                {_id:user._id},
                obj,
                {safe: true, upsert: false, new : true}
            )
            .lean()
            .exec((err, doc)=>{
                if(err)
                    return res.json({status:500, status_message:'something went wrong | internal server error'});
                    // return res.json({status:500, status_message:'something went wrong | internal server error', data:[]});

                delete doc.password;
                return res.json({status:200, status_message: 'success', data:doc});
            })
    }

    getPrizeBonds(req, res, next){  
        let user = res.locals.user;
        _self.Users.find(user._id).lean().exec((err, doc)=>{
            if(err)
                return res.json({status:500, status_message:'something went wrong | internal server error'});
                // return res.json({status:500, status_message:'something went wrong | internal server error', data:[]});
            
            return res.json({status:200, status_message: 'success', data:doc[0].prizebonds});
        })
    }

    postPrizeBonds(req, res, next){
        let user = res.locals.user;
        let {value =``, number =``} = req.body;
        console.log(req.body)

         if(value === '' || number === '')
            return res.json({status:422, status_message:'missing_field'});
            // return res.json({status:422, status_message:'missing_field', data:[]});

        _self.Users.findByIdAndUpdate( user._id,
            {$push: {"prizebonds": {value, number}}},
            {safe: true, upsert: true, new : true},
            (err, doc)=> {
                if(err)
                    return res.json({status:500, status_message:'something went wrong | internal server error'});
                    // return res.json({status:500, status_message:'something went wrong | internal server error', data:[]});
                return res.json({status:200, status_message: 'success', data:doc.prizebonds});
            }
        )
    }

    multipleGetPrizeBonds(req, res, next){
        let user = res.locals.user;
        _self.Users.find(user._id).lean().exec((err, doc)=>{
            if(err)
                return res.json({status:500, status_message:'something went wrong | internal server error'});
                // return res.json({status:500, status_message:'something went wrong | internal server error', data:[]});
            
            return res.json({status:200, status_message: 'success', data:doc[0].prizebonds});
        })
    }

    multiplePostPrizeBonds(req, res, next){

        let user = res.locals.user;
        let {value =``, bondNumber =``, to =``, from=``, inputType=``} = req.body;
        let number = bondNumber;
        let numberArr = [];
        console.log(req.body)

         if(inputType === '' || value === '')
            return res.json({status:422, status_message:'missing_field'});

        if(inputType === 'simple'){
            if(number === '')
                return res.json({status:422, status_message:'missing_field | bondNumber'});
            numberArr = number.split(',');
            let prizebondsArr = [];

            for(var items in numberArr)
                prizebondsArr.push({value, number:parseInt(numberArr[items])})

            _self.Users.findByIdAndUpdate( user._id,
                {$push: {"prizebonds": {$each: prizebondsArr}}},
                {safe: true, upsert: true, new : true},
                (err, doc)=> {
                    if(err)
                        return res.json({status:500, status_message:'something went wrong | internal server error'});
                    return res.json({status:200, status_message: 'success', data:doc.prizebonds});
                }
            )            
        }
        else{
            if(to === '' || from === '')
                return res.json({status:422, status_message:'missing_field | to || from'});

            let b = to;
            to = from;
            from = b;

            let _to = parseInt(to);
            let _from = parseFloat(from);
            let prizebondsArr = [];
            for(_to; _to<=_from; _to++)
                prizebondsArr.push({value, number:_to})

            _self.Users.findByIdAndUpdate( user._id,
                {$push: {"prizebonds": {$each: prizebondsArr}}},
                {safe: true, upsert: true, new : true},
                (err, doc)=> {
                    if(err)
                        return res.json({status:500, status_message:'something went wrong | internal server error'});
                    return res.json({status:200, status_message: 'success', data:doc.prizebonds});
                }
            )       
        }

    }

    getSinglePrizeBonds(req, res, next){  
        let user = res.locals.user;

        _self.Users.find({'prizebonds._id':req.params._id}, 'prizebonds.$').lean().exec((err, doc)=>{
            if(err)
                return res.json({status:500, status_message:'something went wrong | internal server error'});
            
            return res.json({status:200, status_message: 'success', data:doc});
        })
    }

    updateSinglePrizeBonds(req, res, next){
        let user = res.locals.user;
        let {value =``, number =``} = req.body;
        if(value === `` || number === ``)
            return res.json({status:422, status_message:'missing_field'});
            // return res.json({status:422, status_message:'missing_field', data:[]});

            console.log({_id: user._id, 'prizebonds._id':req.params._id})
        _self.Users.findOneAndUpdate(
            {_id: user._id, 'prizebonds._id':req.params._id},
            {"prizebonds.$.value":value,"prizebonds.$.number":number, "prizebonds.$.updated_at":new Date(), "prizebonds.$.winning_count":0, "prizebonds.$.is_winner":false},
            {upsert: false, new:true } 
        )
        .lean()
        .exec((err, doc)=>{
             if(err)
                return res.json({status:500, status_message:'something went wrong | internal server error'});
                // return res.json({status:500, status_message:'something went wrong | internal server error', data:[]});
            
            return res.json({status:200, status_message: 'success', data:doc.prizebonds});

        })
    }

    deleteSinglePrizeBonds(req, res, next){
        let user = res.locals.user;

        _self.Users.update(
            {_id: user._id},
            {$pull:{'prizebonds':{_id:req.params._id}}},
            { safe: true , new: true}
        )
        .lean().exec((err, doc)=>{
            if(err)
                return res.json({status:500, status_message:'something went wrong | internal server error'});
                // return res.json({status:500, status_message:'something went wrong | internal server error', data:[]});

            return res.json({status:200, status_message: 'success'});

        })
    }


    postplayerid(req, res, next){
        let user = res.locals.user;
        let {player_id = ``} = req.body;
        let token = req.get('token');

        if(player_id === ``)
            return res.json({status:422, status_message:'missing_field'});
            // return res.json({status:422, status_message:'missing_field', data:[]});

        _self.Auth.findOneAndUpdate(
            {token},
            {player_id},
            {upsert: false, new:true }            
        )
        .lean()
        .exec((err, doc)=>{
            if(err)
                return res.json({status:500, status_message:'something went wrong | internal server error'});
                // return res.json({status:500, status_message:'something went wrong | internal server error', data:[]});
            return res.json({status:200, status_message: 'success', data:doc});

        })
    }


};


module.exports = apis;