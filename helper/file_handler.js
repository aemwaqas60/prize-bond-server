require('../models/draws.model');
const fs = require('fs'),
	path = require('path'),
	multer = require('multer'),
	main_folder_path = path.join(__dirname, '../files/images/'),
	mongoose = require('mongoose'),
	Draws = mongoose.model('Draws'),
	formidable = require('formidable'),
	shortid = require('shortid');


module.exports = {
	limits: {
		fileSize: 10000000
	},

	deletefile: (file_path, cb) => {
		fs.unlink(file_path, (err) => {
			if (err)
				return console.error(err);
			else
				cb({
					data: 200,
					dataStatus: notify.successNotices.success.value
				});

		});

	},

	fileFilter: () => {
		return (req, file, cb) => {
			console.log('------------ in File Handler   ------------------');
			let mimetype_accept = ['image/jpeg', 'image/jpg', 'image/png', ];
			let extname_accept = ['.jpeg', '.jpg', '.png'];
			if (mimetype_accept.indexOf(file.mimetype) != -1 && extname_accept.indexOf(path.extname(file.originalname).toLowerCase()) != -1)
				cb(null, true)
			else
				cb(new Error('file mimeType is not right extension: ' + path.extname(file.originalname) + '  mimeType: ' + file.mimetype))
		}
	},

	fileSettings: () => {

		return multer.diskStorage({

			destination: (req, file, cb) => {

				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=')
				return cb(null, main_folder_path);
			},

			filename: (req, file, cb) => {

				let tempfilename = shortid.generate();
				req.tempfilename = main_folder_path + tempfilename + path.extname(file.originalname);
				req.tempextn = path.extname(file.originalname);
				console.log({
					tempfilename: req.tempfilename,
					tempextn: req.tempextn
				})
				return cb(null, tempfilename + path.extname(file.originalname));
			}
		})

	},


	file_rename: (current_file, new_file, cb) => {
		fs.open(current_file, 'r', (err, fd) => {
			if (err) {
				if (err.code === "ENOENT") {
					console.error(current_file + ' does not exist');
					return cb(err);
				} else
					return cb(err)
			}
			fs.rename(current_file, new_file, (err) => {
				if (err)
					return cb(err)
				else {
					console.log('changed');
					return cb(null, 'done');
				}
			})
		});
	},

	upload: (req, res, next) => {
		// create an incoming form object
		var form = new formidable.IncomingForm();

		// specify that we want to allow the user to upload multiple files in a single request
		form.multiples = true;

		// store all uploads in the /uploads directory
		form.uploadDir = path.join(__dirname, '../', '/uploads');

		form.on('field', function (name, value) {

			console.log({
				name,
				value
			});
		});

		form.on('fileBegin', function (name, file) {
			console.log(name, file)
		});

		// every time a file has been uploaded successfully,
		// rename it to it's orignal name
		form.on('file', function (field, file) {

			console.log('--file')
			Draws.findByIdAndUpdate({
					_id: res.locals._insertId
				}, {
					extn: path.extname(file.name).toLowerCase(),
					fileOriginalName: file.name
				})
				.lean().
			exec((err, doc) => {
				if (err)
					console.log(err)
				fs.rename(file.path, path.join(form.uploadDir, res.locals._insertId + path.extname(file.name).toLowerCase()));
			});



		});

		// log any errors that occur
		form.on('error', function (err) {
			console.log('An error has occured: \n' + err);
		});

		// once all the files have been uploaded, send a response to the client
		form.on('end', function () {
			console.log('**************************')
			next();
		});

		// parse the incoming request containing the form data
		//form.parse(req);
		form.parse(req, function (err, fields, files) {
			console.log({
				fields: fields,
				files: files
			})

		});

	}

};

function createDirectory(_directory) {
	if (!fs.existsSync(_directory)) {
		fs.mkdirSync(_directory);
		console.log('Directory created at ' + _directory)
	}

}