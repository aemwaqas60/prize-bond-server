const XLSX = require('xlsx');

// parseXLSXfile('df.xlsx',(data)=>{

//     console.log('+++++++++++++++++++++++++++++++++++++++++++')

//     console.log(data);

// });

module.exports = function  parseXLSXfile(file, cb){
   // console.log(file)
    let workbook = XLSX.readFile(file);
    let worksheet = workbook.Sheets[workbook.SheetNames];

   // console.log("file path inside xlsx parser:  "+ file);
    //console.log("workbook:  "+ workbook[0]);
    //console.log(`worksheet:  ${worksheet}`);

    let obj = {
        first_prize: [],
        second_prize: [],
        third_prize: [],
        value :'',
        date:'',
        held_at:'',
        draw_number :'',
        series :'',
    };
    console.log('----------------------');    
    console.log('---------Insdie Xlsparser-------------');
    console.log('----------------------');
    
        let flag ='';
        for(z in worksheet) {
            if(z[0] === '!') continue;
            //parse out the column, row, and value
            var tt = 0;
            for (var i = 0; i < z.length; i++) {
                if (!isNaN(z[i])) {

                   // console.log(`tt:  ${tt}`);
                    tt = i;
                    break;
                }
            }

            let value = worksheet[z].v;

           // console.log(`value:  ${value}`);

            if(value === 'value' || value === 'date' || value === 'held_at' || value === 'draw_number' || value === 'series'){
                flag = value;
                continue;
            }


            if( value === 'first_prize' || value === 'second_prize' || value === 'third_prize'){
                flag = value;
                continue;
            }

            if(flag){
                if (flag === 'first_prize' ||  flag === 'second_prize' || flag === 'third_prize')
                    obj[flag].push(value);
                else
                    obj[flag] = value;
            }
            
        }
        // console.log("---------leaving XLS parseXLSXfile------");
        // console.log("---------Processed object:  " + obj);
        
        return cb(obj);


}