"use strict";
require("../config/mongo");
require("../models/draws.model");
require("../models/user.model");
require("../models/auth.model");

const _ = require("underscore");
const XLSXPARSER = require("./xlsxparser");

let _self;
class admin {
  constructor(path, mongoose, async, objOnesignal) {
    _self = this;
    this.async = async;
    this.main_folder = path.join(__dirname, "../uploads/");
    this.Draws = mongoose.model("Draws");
    this.Users = mongoose.model("User");
    this.Auth = mongoose.model("Auth");
    this.objOnesignal = objOnesignal;
  }

  logout(req, res, next) {
    res
      .clearCookie("_connecti", {
        path: "/"
      })
      .json({
        status: 1,
        status_message: "ok"
      });
  }

  adminDetails(req, res, next) {
    return res.json({
      status: 1,
      status_message: "ok",
      data: req.user
    });
  }
  isAuthenticate(req, res, next) {
    console.log("insdie isAutheeniscate");
    return res.json({
      status: 1,
      status_message: "user authenticated"
    });
  }

  getAppsettings(req, res, next) {}

  postAppsettings(req, res, next) {}

  downloadFile(req, res, next) {
    let {
      id
    } = req.params;

    let filePath = ``;
    let fileName = "download.png";
    console.log({
      filePath,
      fileName
    });

    return res.download(filePath, fileName, err => console.log(err));
  }

  getDraws(req, res, next) {
    _self.Draws
      .find()
      .sort("-created_at")
      .lean()
      .exec((err, doc) => {
        if (err) {
          console.error("error occured during getting data", err);

          res.json({
            status: error
          });
        }

        console.log("----------------");
        console.log("document of  getDraws: " + doc);
        console.log("----------------");

        res.json({
          status: 200,
          status_message: "success",
          data: doc
        });
      });
  }

  processing(req, res, next) {
    console.log("==========Inside processing func==========");

    _self.xlsxParser(req, res, next);
  }

  xlsxParser(req, res, next) {
    let _id = req.params.id;
    console.log("==========Inside xlsparser xls func=========");
    //console.log(_id);

    _self.Draws.find({
      _id: _id
    }).exec((err, doc) => {
      //console.log("document[0]:  " + doc[0]);
      let Drawsobj = doc[0];
      let filepath = _self.main_folder + _id + Drawsobj.extn;
      //console.log("file path is : "+ filepath)
      //   console.log("-------------------------------");
      //   console.log("--------processed data---------");
      //   console.log("-------------------------------");

      XLSXPARSER(filepath, data => {
        // console.log("Data------->", data);
        //console.log( "date:"+ data.date);

        if (data != null) {
          console.log("inside if");

          let first_prize = data.first_prize;
          let second_prize = data.second_prize;
          let third_prize = data.third_prize;
          //   console.log("first_prize:  " + first_prize);
             console.log("second_prize:    " + second_prize);
          //   console.log("third_prize:  " + third_prize);

          _self.Draws
            .findByIdAndUpdate({
              _id: _id
            }, {
              is_processed: true,
              series: data.series,
              value: data.value,
              held_at: data.held_at,
              draw_number: data.draw_number,
              date: data.date,
              prize: [{
                value: "first_prize",
                number: first_prize
              }, {
                value: "second_prize",
                number: second_prize
              }, {
                value: "third_prize",
                number: third_prize
              }]

            })
            .lean()
            .exec((err, doc) => {
              if (err) {
                console.log("error occured during updation" + err);
                res.json({
                  status: "unsuccess",
                  status_message: "fileupdation error"
                });
              }

              res.json({
                status: "success",
                status_message: "file successully processed"
              });
            });
        }
      });

      console.log("-------leaving xlsparser----------");
    });
  }

  sendnotif(req, res, next) {
    _self.finduserprizebonds(req, res, next);
  }

  finduserprizebonds(req, res, next) {
    let _id = req.params.id;
    _self.Draws.find({
      _id: _id
    }).exec((err, doc) => {
      if (err) return;
      let Drawsobj = doc[0];
      let first_prize = doc[0].prize[0].number;
      let second_prize = doc[0].prize[1].number;
      let third_prize = doc[0].prize[2].number;
      let first_prize_value = doc[0].prize[0].value;
      let second_prize_value = doc[0].prize[1].value;
      let third_prize_value = doc[0].prize[2].value;
      let results = {};

      _self.async.parallel(
        [
          function (callback) {
            _self.Users
              .find({
                "prizebonds.value": parseInt(Drawsobj.value),
                "prizebonds.number": first_prize
              })
              .lean()
              .exec((err, doc) => {
                if (err) callback(err);

                console.log("inside executions");
                callback(null, doc);
                _self.objOnesignal.send(
                  "First Winner", {
                    as: "as"
                  },
                  "all", [],
                  (err, result) => callback(null, "done")
                );
              });
          },
          /* function (callback) {
            _self.Users
              .aggregate([{
                  $unwind: "$prizebonds"
                },
                {
                  $match: {
                    "prizebonds.value": parseInt(Drawsobj.value),
                    "prizebonds.number": {
                      $in: second_prize
                    }
                  }
                }
              ])
              .exec((err, doc) => {
                if (err) callback(err);

                _self.Auth
                  .find({
                    _user: {
                      $in: doc
                    }
                  })
                  .lean()
                  .exec((err, doc) => {
                    if (err)
                      return res.json({
                        status: 500,
                        status_message: "something went wrong | internal server error",
                        data: []
                      });
                    let second_winner_player_ids = [];
                    for (let rows in doc) {
                      if (doc[rows].player_id !== undefined)
                        second_winner_player_ids.push(doc[rows].player_id);
                    }

                    _self.objOnesignal.send(
                      `Congratulations !, Your bond ${second_prize} won 2nd prize of Rs ${second_prize_value} `, {
                        as: "as"
                      },
                      "players",
                      second_winner_player_ids,
                      (err, result) => callback(null, "done")
                    );
                  });
              });
          } */
          /* ,
                    function (callback) {
                      _self.Users
                        .aggregate([{
                            $unwind: "$prizebonds"
                          },
                          {
                            $match: {
                              "prizebonds.value": parseInt(Drawsobj.value),
                              "prizebonds.number": {
                                $in: third_prize
                              }
                            }
                          }
                        ])
                        .exec((err, doc) => {
                          if (err) callback(err);

                          let third_winner_player_ids = [];

                          _self.Auth
                            .find({
                              _user: {
                                $in: doc
                              }
                            })
                            .lean()
                            .exec((err, doc) => {
                              if (err)
                                return res.json({
                                  status: 500,
                                  status_message: "something went wrong | internal server error",
                                  data: []
                                });

                              for (let rows in doc) {
                                if (doc[rows].player_id !== undefined)
                                  third_winner_player_ids.push(doc[rows].player_id);
                              }
                              console.log(third_winner_player_ids);

                              _self.objOnesignal.send(
                                `Congratulations !, Your bond ${third_prize} won 3rd prize of Rs ${third_prize_value} `, {
                                  data: "as"
                                },
                                "players",
                                third_winner_player_ids,
                                (err, result) => callback(null, "done")
                              );
                            });
                        });
                    } */
        ],
        // optional callback
        function (err, results) {
          if (err)
            return res.json({
              status: 500,
              status_message: "something went wrong | internal server error",
              data: []
            });

          return res.json({
            status: 200,
            status_message: "success",
            data: results
          });




          //_self.objOnesignal.send('First Winner', {as:'as'}, 'all');
          //console.log(results[1])
          // let second_winners = results[1];
          // let third_winners = results[2];
          // let second_winner_player_ids = [];
          // let third_winner_player_ids = [];

          // 	_self.Auth.find(
          // 		{'_user': {$in:second_winners}}
          // 	)
          // 	.lean()
          // 	.exec((err, doc)=>{
          // 		if(err)
          //         	return res.json({status:500, status_message:'something went wrong | internal server error', data:[]});
          // 		console.log(doc)
          // 		for(let rows in doc){
          // 			second_winner_player_ids.push(doc[rows].player_id);
          // 		}

          // 	console.log(second_winner_player_ids);

          // })

          // _self.Auth.find(
          // 	{'_user': {$in:third_winners}}
          // )
          // .lean()
          // .exec((err, doc)=>{
          // 	if(err)
          // 		return res.json({status:500, status_message:'something went wrong | internal server error', data:[]});
          // 	console.log(doc)
          // 	for(let rows in doc){
          // 		third_winner_player_ids.push(doc[rows].player_id);
          // 	}

          // console.log(third_winner_player_ids);

          // })
          //_self.objOnesignal.send('second Winner', {as:'as'}, 'players', [])

          //return res.json({status:200, status_message:'ok', data:results});

          // the results array will equal ['one','two'] even though
          // the second function had a shorter timeout.
        }
      );

      // _self.Users.find(
      // 	{'prizebonds.value': parseInt(Drawsobj.value), 'prizebonds.number':{$in :doc[0].prize[1].number}}
      // )
      // .lean().exec((err, doc)=>{
      // 	if(err)
      // 		console.log(err)
      // 	console.log(doc)

      // })

      // _self.Users.aggregate([
      // 	{$unwind:"$prizebonds"},
      // 	{$match: { 'prizebonds.value': parseInt(Drawsobj.value)}},
      // 	{ "$group": {'_id': '$_id', "prizebonds": { "$addToSet": "$prizebonds" }}},
      // 	{ "$project": {"_id": 1, "prizebonds":1}},
      // ])
      // .exec((err, doc)=>{

      // 	for(let users in doc){
      // 		let user = doc[users].prizebonds;
      // 		console.log(user)
      // 		for(let prizebonds in user){
      // 			let prizebond = user[prizebonds];
      // 			console.log(prizebond.number)

      // 			_self.Draws.find(
      // 				{_id:_id, 'prize.0.number':parseInt(prizebond.number)}
      // 			)
      // 			.exec((err, doc)=>{
      // 				if(err)
      // 					console.log(err)
      // 				console.log(doc)
      // 			})

      // 		}
      // 	}
      // 	res.json(doc)
      // })
    });
  }

  testsendnotif(req, res, next) {
    _self.objOnesignal.send(
      "Third Winner", {
        data: "as"
      },
      "players",
      req.params.player,
      (err, result) => console.log(result)
    );
  }

  sendNotifications(req, res, next) {
    let message = req.body.message;
    let playerId = req.body.id;
    let isBroadcast = req.body.isBroadcast;


    console.log("====================================");
    console.log("======Inside send notifications=====");
    console.log("====================================");

    //console.log("player id:",playerId);
    //console.log("message:",message);

    console.log("body:----->>>>", req.body);



    //sending  notification to  single user
    if ((isBroadcast === 'false') && !(!message || 0 === message.length) && !(!playerId || 0 === playerId.length)) {
      console.log("----inside single user notifications----");
      // console.log("message :" + message);
      //console.log("playerId :" + playerId);

      _self.objOnesignal.send(
        message, {
          data: "as"
        },
        "players", [playerId],
        (err, result) => {
          if (err) {
            res.json({
              status: 500,
              status_message: "something went wrong | internal server error",
              error: err
            });
          }
          res.json({
            status: 1,
            status_message: "message successfully sent",
          });
          //console.log(result);
        }
      );

      // sending notifications to all users 
    } else if ((isBroadcast === 'true') && !(!message || 0 === message.length)) {
      // broadcast notification 
      console.log("-----inside broadcast notifications------");
      //console.log("message :" + message);
      //console.log("playerId :" + playerId);

      _self.objOnesignal.send(message, {
          data: "as"
        }, "all", [], (err, result) => {
          if (err) {
            return res.json({
              status: 500,
              status_message: "something went wrong | internal server error",
              error: err
            });
          }
          return res.json({
            status: 1,
            status_message: "message successfully sent",
          });
          console.log(result)
        }

      );
    } else {
      console.log("-----inside else------");
      res.json({
        status: 500,
        status_message: "Invlid input field error.",
      });
    }
  }

  postDraws(req, res, next) {
    console.log("==========Inside PostDraw func=========");
    _self.Draws
      .find({
        _id: res.locals._insertId
      })
      .lean()
      .exec((err, doc) => {
        if (err)
          return res.json({
            status: 500,
            status_message: "something went wrong | internal server error",
            data: []
          });

        return res.json({
          status: 1,
          status_message: "file successfully uploaded",
          data: doc
        });
      });
  }
}

module.exports = admin;