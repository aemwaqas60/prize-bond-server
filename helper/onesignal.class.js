const https = require('https');

let _self;
class onesignal{
    constructor(app_id, Authorization){
        _self = this;
        this.app_id = app_id;
        this.host = "onesignal.com";
        this.port = 443;
        this.header = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": Authorization,
        }
    }

    _message(_content, _data, type, players){
        console.log(players);

        let obj = {
            app_id: _self.app_id,
            headings:{"en": "Savings-App Title"}, //{"en": "English Title", "es": "Spanish Title"}
            contents: {"en": _content}, //{"en": "English Message", "es": "Spanish Message"}
            data: _data, //{"abc": "123", "foo": "bar"}
        };
        if(type === 'all')
            obj.included_segments =  ["All"];
        if(type === 'players')
            obj.include_player_ids = players;

        return obj;
    }
    
    send(content, _data, type, players, cb){

        let data = _self._message(content, _data, type, players);
        let options = {
            host: _self.host,
            port: _self.port,
            path: "/api/v1/notifications",
            method: "POST",
            headers: _self.header
        }

        let req = https.request(options, function(res) {  
        res.on('data', function(data) {
            console.log("Response:");
            console.log(JSON.parse(data));
            });
        });
    
        req.on('error', function(e) {
            console.log("ERROR:");
            console.log(e);
        });
    
        req.write(JSON.stringify(data));
        req.end();
        cb(null, 'done')
    }


}

module.exports = onesignal;