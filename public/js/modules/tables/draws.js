"use strict";
(function () {
    angular.module('blankonApp.tables.draws', ['ui.bootstrap', 'angularSpinner', 'angularMoment', 'localytics.directives', 'checklist-model'])
        .constant('DATETIME_FORMAT', "DD MMM YY | hh:mm A")
        .directive('fileModel', fileModel)
        .controller('TableCtrl_Draws', TableCtrl_Draws)

        .controller('CtrlAddFile', CtrlAddFile)
        .service('SharedDataService', SharedDataService);
})();

function SharedDataService() {
    var products = [];
    var fieldsObj = {};
    var draws = [];
    this.setProducts = function (data) {
        products = data;
    };
    this.getProducts = function () {
        return products;
    };
    this.setfieldObj = function (data) {
        fieldsObj = data;
    }
    this.getfieldObj = function () {
        return fieldsObj;
    }
    this.setdraws = function (data) {
        draws = data
    }
    this.getdraws = function () {
        return draws;
    }
}

function fileModel($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            element.bind('change', function () {
                scope.$apply(function () {
                    var re = /(\.xls|\.xlsx)$/i;
                    if (element[0].files[0]) {
                        if (!re.exec(element[0].files[0].name)) {
                            alert("File extension not supported!");
                            return;
                        }
                    }
                    // console.log(element[0].files[0])
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}

function TableCtrl_Draws($scope, $log, $http, settings, _request, $modal, $window, DATETIME_FORMAT, alertFactory, SharedDataService) {


    _request.get('/admin/draws', {}, function (data) {
        console.log("==========================================");
        console.log("=========Inside Get--> /admin/draws=========");
        console.log("==========================================");

        $scope.draws = data.data;
        console.log("Data:  ", data.data)
        SharedDataService.setdraws(data.data);
    })

    $scope.processing = function (data) {
        //console.log("is processed: ", data.is_processed);
        if (data.is_processed)
            return;

        // console.log("before sending process request");
        //console.log("data.id: " + data._id);

        _request.get('/admin/processing/' + data._id, '', function (_data) {


            console.log("===========================================");
            console.log("====Inside Get---> /admin/processing/======");
            console.log("===========================================");

            console.log("respose data:" + _data.data);



            _request.get('/admin/draws', {}, function (data1) {

                $scope.draws = data1.data;
               // console.log("Data:  ", data1.data)
                SharedDataService.setdraws(data1.data);

                // var result = _data.data;
                // for (var items in $scope.draws) {
                //     if ($scope.draws[items]._id === data._id) {
                //         $scope.draws[items].prize = result.prize;
                //         SharedDataService.setdraws($scope.draws);
                //     }
                // }
            })

            alertFactory._alert('SuccessFully File Processed');
            //console.log("successsfully file processed");
        })
    }

    $scope.notif = function (data) {
        if (data.is_processed === false)
            return;

         console.log("id is:",data._id);

        _request.get('/admin/sendnotif/' + data._id, '', function (_data) {
            console.log(_data)
            alertFactory._alert('SuccessFully Sent Notifications');
        });
    }

    $scope.add = function (data) {
        $modal.open({
            templateUrl: 'AddForm.html', // loads the template
            backdrop: true, // setting backdrop allows us to close the modal window on clicking outside the modal window
            windowClass: 'modal', // windowClass - additional CSS class(es) to be added to a modal window template
            size: 'lg',
            controller: CtrlAddFile,
        })
    }

}


function CtrlAddFile($scope, $modalInstance, $log, alertFactory, _request, SharedDataService) {

    console.log("===================================");
    console.log("=====Inside Add file controller=====");
    console.log("====================================");

    $scope.title = "Add File";
    $scope.imageButtons = true;

    $scope.submit = function (data) {

        var fd = new FormData();
        //fd.append('name', data.name);
        fd.append('xlsfile', $scope.xlsfile);

        _request.postFile('/admin/draws', fd, function (data) {
            console.log("====================================");
            console.log("=====Inside Post--> /admin/draws=====");
            console.log("=====================================");
            $scope.title = "Add File";
            // console.log("Response data:", data);    

            var draws = SharedDataService.getdraws();
            // console.log(draws);
            draws.splice(0, 0, data.data[0])
            alertFactory._alert('SuccessFully Added');
            //console.log($scope.draws)
            $modalInstance.dismiss('cancel');
        })
    }
}