"use strict";
(function () {
    angular.module('blankonApp.forms.notification', [])


        .service('_req', function ($rootScope, $http, $location, $window) {

            // this.get = function (route, cb) {
            //     $http.get(route) // Simple GET request example :
            //     .success(function(response) {
            //         return cb(response.data);
            //     })
            //     .error(function(data, status, headers, config) {
            //         console.log(data)
            //         console.log(status)
            //         if(status === 401){
            //             window.location = "/account.html#/sign-in";
            //         }

            //         // called asynchronously if an error occurs
            //         // or server returns response with an error status.
            //     });
            // }

            this.post = function (route, params, cb) {
                $http({
                    method: 'POST',
                    url: route,
                    headers: {
                        "Content-type": 'application/json'
                    },
                    data: params

                }).then(function (response) {
                    // console.log(response)
                    return cb(response.data);

                }, function (response) {
                    console.log('i am in error');
                });
            }
        })


        .controller('NotificationCtrl', function ($scope, $http, settings, _req, $window, $modal) {

            //var _name, _email;

            // _req.get('/admin/profile', function (response){
            //     console.log(response)
            //     $scope.user = {
            //         name:response.name,
            //         email:response.email,
            //         role:(response.rid===1?'Admin':'Client'),
            //         username:response.username,
            //         password:''
            //     }
            //     _name=response.name;
            //     _email=response.email;
            // });
            $scope.form_submit = function (data) {
                // console.log(data);



                _req.post('/admin/notification', data, function (response) {
                    // console.log(response)
                    if (response.status == 1) {
                        //console.log(response.status_message)
                        // $scope._error = response.status_message
                        //$scope._isText_fill = true;
                        //alert("message sent successfully");
                        $scope.user.id = "";
                        $scope.user.message = "";
                        console.log(response)
                        $("#AlertBox").text(response.status_message)
                        $("#AlertBox").fadeIn();

                        $window.setTimeout(function () {
                            $("#AlertBox").fadeOut(300)
                        }, 3000);


                        return;
                    } else {
                        console.log("something went wrong");

                        $("#AlertBoxUnsuccess").text(response.status_message)
                        $("#AlertBoxUnsuccess").fadeIn();

                        $window.setTimeout(function () {
                            $("#AlertBoxUnsuccess").fadeOut(300)
                        }, 3000);
                    }
                });




                //  $modal.open({
                //     templateUrl: 'profile.html', // loads the template
                //     backdrop: true, // setting backdrop allows us to close the modal window on clicking outside the modal window
                //     windowClass: 'modal', // windowClass - additional CSS class(es) to be added to a modal window template
                //     controller: function ($scope, $modalInstance, $log) {

                //         console.log(data)
                //         $scope.passCheck = function(obj){

                //             $scope._isText_fill =false;

                //             if(!obj || !obj.pass1){
                //                 console.log('tjhere')
                //                 $scope._error = "please fill all fields";
                //                 $scope._isText_fill = true;
                //                 return ;
                //             }

                //             data.oldpass = obj.pass1;
                //             _req.post('/admin/profile', data, function (response){
                //                 console.log(response)
                //                 if(response.status == 510){
                //                     console.log(_name)
                //                     $scope._error = response.status_message
                //                     $scope._isText_fill = true;

                //                     return ;
                //                 }

                //                 $("#AlertBox").text('Successfully Updated')
                //                 $("#AlertBox").fadeIn();
                //                 $window.setTimeout(function () {
                //                     $("#AlertBox").fadeOut(300)
                //                 }, 3000);
                //                 $scope.user.name = data.name;
                //                 $scope.user.email = data.email;
                //                 $scope.user.password = '';
                //                 $modalInstance.dismiss('cancel');

                //             });


                //         }
                //         $scope.cancel = function () {
                //             $modalInstance.dismiss('cancel'); 
                //         };
                //     }

                // });

            }
        });

})();