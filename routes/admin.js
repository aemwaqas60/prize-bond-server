'use strict';
const express = require('express'),
	router = express.Router(),
	multer = require('multer'),
	file_handler = require('../helper/file_handler'),
	adminClass = require('../helper/admin.class'),
	formidable = require('formidable');
const fs = require('fs');
const Draws = require('mongoose').model('Draws');


module.exports = (ObjSalthashpassword, objAuth, async, moment, path, mongoose, objOnesignal, _) => {

	let objadminClass = new adminClass(path, mongoose, async, objOnesignal);

	router.use('/', objAuth.isAuthenticated);

	router.get('/admin_authentication', objadminClass.isAuthenticate);

	router.get('/logout', objadminClass.logout);

	router.get('/details', objadminClass.adminDetails);


	// router.route(`/draws`)
	// 	.post(multer({fileFilter:file_handler.fileFilter(),limits:file_handler.limits,storage:file_handler.fileSettings()}).single('xlsfile'), objadminClass.postDraws)

	router.get('/draws', objadminClass.getDraws);

	router.get('/processing/:id', objadminClass.processing);

	router.post('/draws', (req, res, next) => {
		let obj = new Draws({});
		obj.save((err, doc, numAffected) => {
			if (err)
				return cb(err, "error in query");
			res.locals._insertId = doc._id;
			next();
		});
	}, file_handler.upload, objadminClass.postDraws);


	router.post('/notification', objadminClass.sendNotifications);


	router.get('/xlsxparser/:id', objadminClass.xlsxParser);

	router.get('/sendnotif/:id', objadminClass.sendnotif);
	router.get('/finduserprizebonds/:id', objadminClass.finduserprizebonds);

	router.get('/testsendnotif/:player', objadminClass.testsendnotif);

	router.route('/appsettings')
		.get(objadminClass.getAppsettings)
		.post(objadminClass.postAppsettings);


	return router
};