'use strict';
const express = require('express'),
    router = express.Router(),
	apisClass = require('../helper/apis.class'),
    multer = require('multer'),
    file_handler = require('../helper/file_handler')

const mongoose = require('mongoose');

module.exports = (ObjSalthashpassword, objAuth, async, moment, path,  _, jwt, jwtSecret) =>{

	let objapisClass = new apisClass (mongoose, ObjSalthashpassword, jwt, jwtSecret);

	router.use('/', (req, res, next)=>{
        console.log('----------- APIS -------');
        next();
    });


    router.post('/users/signup', objapisClass.signup);
    router.post('/users/signin', objapisClass.signin);

    
    

    router.post('/users/playerid', objapisClass._isAuthenticate, objapisClass.postplayerid);

    router.route('/users/profile')
        .all(objapisClass._isAuthenticate)
        .get(objapisClass.getuserprofile)
        //.put(objapisClass._edituserprofile)
    
    router.post('/users/profile', objapisClass._isAuthenticate, multer({fileFilter:file_handler.fileFilter(),limits:file_handler.limits,storage:file_handler.fileSettings()}).single('file'), objapisClass.edituserprofile)

    router.route('/users/prizebonds')
        .all(objapisClass._isAuthenticate)
        .get(objapisClass.getPrizeBonds)
        .post(objapisClass.postPrizeBonds)

    router.route('/users/multipleprizebonds')
        .all(objapisClass._isAuthenticate)
        .get(objapisClass.multipleGetPrizeBonds)
        .post(objapisClass.multiplePostPrizeBonds)

    router.route('/users/prizebonds/:_id')
        .all(objapisClass._isAuthenticate)
        .get(objapisClass.getSinglePrizeBonds)
        .put(objapisClass.updateSinglePrizeBonds)
        .delete(objapisClass.deleteSinglePrizeBonds)

	return router
};