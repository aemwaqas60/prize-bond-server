const XLSX = require('xlsx');

parseXLSXfile('df.xlsx',(data)=>{

    console.log('+++++++++++++++++++++++++++++++++++++++++++')

    console.log(data);

});

function parseXLSXfile(file, cb){
    console.log(file)
    let workbook = XLSX.readFile(file);
    let worksheet = workbook.Sheets[workbook.SheetNames];

    let obj = {
        first_prize: [],
        second_prize: [],
        third_prize: [],
        value :'',
        date :'',
        held_at :'',
        draw_number :'',
        series :'',
    };
    console.log('----------------------')
        let flag ='';
        for(z in worksheet) {
            if(z[0] === '!') continue;
            //parse out the column, row, and value
            var tt = 0;
            for (var i = 0; i < z.length; i++) {
                if (!isNaN(z[i])) {
                    tt = i;
                    break;
                }
            }
            let value = worksheet[z].v;

            if(value === 'value' || value === 'date' ||value === 'held_at' ||value === 'draw_number' ||value === 'series'){
                flag = value;
                continue;
            }


            if(value === 'second_prize' || value === 'third_prize' || value === 'first_prize'){
                flag = value;
                continue;
            }

            if(flag){
                if(flag === 'second_prize' || flag === 'third_prize' || flag === 'first_prize')
                    obj[flag].push(value);
                else
                    obj[flag] = value;

            }
            
        }
        
        return cb(obj);
}